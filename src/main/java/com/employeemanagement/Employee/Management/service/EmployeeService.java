package com.employeemanagement.Employee.Management.service;

import java.util.List;

import com.employeemanagement.Employee.Management.model.Employee;

//One Service we will use all
public interface EmployeeService {
    
    public List<Employee> readAllEmployee();

    public Employee readOneEmployee(Long id);

    public Employee addEmployee(Employee employee);

    public Employee findEmployeeUpdate(Long id, Employee employee);

    public void removeEmployeeById(Long id);

    public void removeAllEmployee();
}
