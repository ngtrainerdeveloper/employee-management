package com.employeemanagement.Employee.Management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.employeemanagement.Employee.Management.ResponseMessage.Response;
import com.employeemanagement.Employee.Management.model.Employee;
import com.employeemanagement.Employee.Management.service.EmployeeService;

@RestController
@RequestMapping(path = "/employees")
public class MainController {

    private EmployeeService eService;

    @Autowired
    public MainController(EmployeeService eService) {
        this.eService = eService;
    }

    @GetMapping("/")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        List<Employee> employees = eService.readAllEmployee();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getOneEmployee(@PathVariable("id") Long id) {
        Employee employee = eService.readOneEmployee(id);
        if (employee == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }
    }

    @PostMapping("/add-employee")
    public ResponseEntity<Response> addEmployee(@RequestBody Employee employee) {
        eService.addEmployee(employee);
        Response responseMessage = new Response("Employee added with ID " + employee.getId(), HttpStatus.CREATED);
        return new ResponseEntity<>(responseMessage, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") Long id, @RequestBody Employee employee) {
        eService.findEmployeeUpdate(id, employee);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> removeOneEmployee(@PathVariable("id") Long id) {
        eService.removeEmployeeById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/all")
    public ResponseEntity<Employee> removeAllEmployee() {
        eService.removeAllEmployee();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
