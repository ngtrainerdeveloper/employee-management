package com.employeemanagement.Employee.Management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeemanagement.Employee.Management.model.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{
}
