package com.employeemanagement.Employee.Management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeemanagement.Employee.Management.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{
}
