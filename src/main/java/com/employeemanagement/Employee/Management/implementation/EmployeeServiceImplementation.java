package com.employeemanagement.Employee.Management.implementation;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.employeemanagement.Employee.Management.exception.EmployeeNotFoundException;
import com.employeemanagement.Employee.Management.model.Client;
import com.employeemanagement.Employee.Management.model.Company;
import com.employeemanagement.Employee.Management.model.Employee;
import com.employeemanagement.Employee.Management.model.Products;
import com.employeemanagement.Employee.Management.model.Skills;
import com.employeemanagement.Employee.Management.repository.ClientRepository;
import com.employeemanagement.Employee.Management.repository.CompanyRepository;
import com.employeemanagement.Employee.Management.repository.EmployeeRepository;
import com.employeemanagement.Employee.Management.repository.ProductRepository;
import com.employeemanagement.Employee.Management.repository.SkillsRepository;
import com.employeemanagement.Employee.Management.service.EmployeeService;

@Service
public class EmployeeServiceImplementation implements EmployeeService {

    @Autowired
    private EmployeeRepository eRepository;

    @Autowired
    private SkillsRepository sRepository;

    @Autowired
    private CompanyRepository cRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ProductRepository pRepository;

    // Read All Employees
    @Override
    public List<Employee> readAllEmployee() {
        List<Employee> employees = eRepository.findAll();
        return employees;
    }

    @Override
    public Employee addEmployee(Employee employee) {
        return eRepository.save(employee);
    }

    @Override
    public Employee findEmployeeUpdate(Long id, Employee employee) {
        JpaRepository<?, ?>[] repositories = { eRepository, sRepository, cRepository, pRepository, clientRepository };

        for (JpaRepository<?, ?> repository : repositories) {
            repository.flush();
        }

        Employee existEmployee = eRepository.findEmployeeById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("employee by id " + id + " was not found."));

        String[] fields = { "firstname", "lastname", "email", "phone", "team", "dateOfJoining", "skills", "company",
                "client" };
        Object[] values = { employee.getFirstname(), employee.getLastname(), employee.getEmail(), employee.getPhone(),
                employee.getTeam(), employee.getDateOfJoining(), employee.getSkills(), employee.getCompany(),
                employee.getClient() };
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            Object value = values[i];
            try {
                {
                    /*
                     * This line of code is using reflection in Java to dynamically retrieve the
                     * setter method of a given field in the existEmployee object.
                     * 
                     * The getMethod() method takes two parameters: the name of the method to be
                     * retrieved (in this case, a setter method), and the parameter types of the
                     * method.
                     * 
                     * The method name is constructed by concatenating the string "set" with the
                     * capitalized name of the field (with the first letter capitalized). For
                     * example, if field is "firstname", then the method name will be
                     * "setFirstname".
                     * 
                     * The value.getClass() call retrieves the type of the value, which is needed as
                     * a parameter type for the getMethod() method.
                     * 
                     * Overall, this code allows you to dynamically retrieve the setter method for
                     * any field in the existEmployee object, based on the name of the field passed
                     * in as a string.
                     */}
                Method method = existEmployee.getClass()
                        .getMethod("set" + field.substring(0, 1).toUpperCase() + field.substring(1), value.getClass());
                method.invoke(existEmployee, value);
            } catch (Exception e) {
                // handle the exception here
            }
        }

        for (JpaRepository<?, ?> repository : repositories) {
            repository.flush();
        }

        Employee updateEmployee = eRepository.save(existEmployee);
        return eRepository.save(updateEmployee);
    }

    @Override
    @Transactional
    public void removeEmployeeById(Long id) {
        Employee existEmployee = eRepository.findEmployeeById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("employee by id " + id + " was not found."));

        // Delete employee
        eRepository.delete(existEmployee);
    }

    @Override
    @Transactional
    public void removeAllEmployee() {
        JpaRepository<?, ?>[] repositories = { clientRepository, pRepository, cRepository, sRepository, eRepository };

        for (JpaRepository<?, ?> repository : repositories) {
            repository.deleteAll();
        }
    }

    @Override
    public Employee readOneEmployee(Long id) {
        return eRepository.findEmployeeById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("employee by id " + id + " was not found."));
    }

}
