package com.employeemanagement.Employee.Management.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"lastname","email", "phone"})})
public class Employee {
    @JsonIgnore
    @Transient
	int Min = 100000;
	@JsonIgnore
    @Transient
	int Max = 999999;
    @Id
    @Column(updatable = false)
    private Long id = (long) Math.floor(Math.random() * (Max - Min) + Min);
    private String firstname;
    private String lastname;
    private String email;
    private String phone;
    private String team;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "IST")
    private Date dateOfJoining;

    @OneToMany(targetEntity = Skills.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "es_fk", referencedColumnName = "id")
    private List<Skills> skills;

    @OneToOne(targetEntity = Company.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ec_fk", referencedColumnName = "id")
    private Company company;


    @OneToMany(targetEntity = Client.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ecl_fk", referencedColumnName = "id")
    private List<Client> client;
}
