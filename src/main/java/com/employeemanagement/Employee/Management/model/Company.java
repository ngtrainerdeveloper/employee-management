package com.employeemanagement.Employee.Management.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    private String company_name;
    private String postal_code;
    private String company_url;
    
    @OneToMany(targetEntity = Products.class, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ep_fk", referencedColumnName = "id")
    private List<Products> products;
}
