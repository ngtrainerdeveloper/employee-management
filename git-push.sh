#!/bin/bash

#!Add the file from directory to gitlab
git add .

#!Find the file .DS_Store from directory, then remove it befor moving to gitlab
find . -type f -name '.DS_Store' -exec git rm --cached {} \;

#!Checking the files which are added/removed
git status

#!Reads the input from user in CI and save it to commitMessage variable
read -p "Enter commit message: " commitMessage

#!Commit with commitMessage to gitlab
git commit -m "$commitMessage"

#!Push the file from directory to gitlab
git push -u origin main

#!If successfully added to gitlab show the message on screen to understand.
echo "Script executed successfully."
