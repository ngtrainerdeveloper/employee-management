# Backend Server

## REST API -  - Server {http://localhost:81/}

## Http Methods
## ---------------------------------------------------------

### Get All - http://localhost:81/employees

### Get One - http://localhost:81/employees/{id}

### Create Employee - http://localhost:81/add-employee

### Update Employee - http://localhost:81/employees/{id}

### Remove All - http://localhost:81/employees/all

### Remove One - http://localhost:81/employees/{id}

## ---------------------------------------------------------
### by - Raj Dutta (Spring Boot and Java)